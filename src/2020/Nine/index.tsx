import React, { FunctionComponent } from 'react';
import { Outcome } from '../../components';
import input from './input';

interface Props {}

const Nine: FunctionComponent<Props> = () => {
  const input = [
    35,
    20,
    15,
    25,
    47,
    40,
    62,
    55,
    65,
    95,
    102,
    117,
    150,
    182,
    127,
    219,
    299,
    277,
    309,
    576,
  ];
  // const preambleLength = 25;
  // const considerPrevious = 25;
  const preambleLength = 5;
  const considerPrevious = 5;

  const preambleSumsToResult = (
    preamble: number[],
    result: number
  ): boolean => {
    const number = preamble.find((firstNumber) => {
      const preambleWithouFirstNumber = preamble.filter(
        (secondNumber) => secondNumber !== firstNumber
      );
      return !!preambleWithouFirstNumber.find(
        (secondNumber) => firstNumber + secondNumber === result
      );
    });

    if (!number) {
      return false;
    }
    return true;
  };

  const findNumber = (puzzleInput: number[]): number | undefined => {
    return puzzleInput.find((_number, index) => {
      if (index < preambleLength) return false;

      const preamble = puzzleInput.slice(index - considerPrevious, index);

      return !preambleSumsToResult(preamble, puzzleInput[index]);
    });
  };

  const firstAnswer = findNumber(input);

  const setLeadsToSum = (
    puzzleInput: number[],
    indexStart: number,
    initialIndexEnd: number,
    sumToFind: number
  ): boolean => {
    let indexEnd = initialIndexEnd;
    let acc = input[indexStart];

    while (acc < sumToFind) {
      acc += puzzleInput[indexEnd];
      indexEnd++;
    }

    console.log(indexStart, indexEnd, acc);

    if (acc === sumToFind) {
      console.log('FOUND!');
      const outcomeArray = input.splice(indexStart, indexEnd - 2);
      console.log(outcomeArray);

      console.log(
        outcomeArray.sort()[0] + outcomeArray.sort()[outcomeArray.length - 1]
      );
      return true;
    } else if (acc > sumToFind) {
      return false;
    }

    return false;
  };

  const findSetForSum = (puzzleInput: number[], sumToFind: number) => {
    return puzzleInput.find((_number, i) => {
      setLeadsToSum(puzzleInput, i, i + 1, sumToFind);
    });
  };

  const test = findSetForSum(input, firstAnswer as number);

  console.log(test);

  return <Outcome firstOutcome={firstAnswer} />;
};

export default Nine;
