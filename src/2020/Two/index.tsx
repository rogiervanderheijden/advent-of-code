import React from 'react';
import Outcome from '../../components/Outcome';
import data from './data';

interface Props {}

type DataSingleLine = [[number, number], string, string];

interface State {
  firstOutcome: number | null;
  secondOutcome: number | null;
  data: DataSingleLine[];
}

class Two extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      firstOutcome: null,
      secondOutcome: null,
      data: data as DataSingleLine[],
    };
  }

  componentDidMount() {
    let total = 0;
    let secondTotal = 0;

    this.state.data.forEach((line: DataSingleLine) => {
      if (this.isValidOne(line)) {
        total += 1;
      }
    });

    this.state.data.forEach((line: DataSingleLine) => {
      if (this.IsValidTwo(line)) {
        secondTotal += 1;
      }
    });

    this.setState({
      firstOutcome: total,
      secondOutcome: secondTotal,
    });
  }

  private isValidOne = (line: DataSingleLine) => {
    let occurences = 0;
    Array.from(line[2]).forEach((letter) => {
      if (letter === line[1]) {
        occurences += 1;
      }
    });
    if (occurences >= line[0][0] && occurences <= line[0][1]) {
      return true;
    }
    return false;
  };

  private IsValidTwo = (line: DataSingleLine) => {
    const letterArray = Array.from(line[2]);
    const firstCheckPosition = line[0][0] - 1;
    const secondCheckPosition = line[0][1] - 1;

    if (
      (letterArray[firstCheckPosition] === line[1] &&
        letterArray[secondCheckPosition] !== line[1]) ||
      (letterArray[firstCheckPosition] !== line[1] &&
        letterArray[secondCheckPosition] === line[1])
    ) {
      return true;
    }
    return false;
  };

  render() {
    return (
      <Outcome
        firstOutcome={this.state.firstOutcome}
        secondOutcome={this.state.secondOutcome}
      />
    );
  }
}

export default Two;
