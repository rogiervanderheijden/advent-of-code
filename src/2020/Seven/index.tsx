import React, { FunctionComponent } from 'react';
import { Outcome } from '../../components';
import input from './input';

interface Props {}

interface SingleLineRule {
  [key: string]: number;
}

interface SingleRuleObj {
  [key: string]: SingleLineRule;
}

const Seven: FunctionComponent<Props> = () => {
  const createRuleObject = (ruleRaw: string): Array<SingleRuleObj> => {
    const stringToCut = ' bags contain ';
    const containerBag: string = ruleRaw.substring(
      0,
      ruleRaw.indexOf(stringToCut)
    );

    const rest = ruleRaw
      .substring(ruleRaw.indexOf(stringToCut) + stringToCut.length)
      .split(',');

    const subBags = rest
      .map((singleRulesRaw: string) => {
        return singleRulesRaw.trim().split(/\s+/).slice(0, 3);
      })
      .reduce((acc, value) => {
        if (value.length !== 3) {
          throw new Error(`value length !== 3, value: ${value}`);
        }
        const subBagRuleObject: SingleLineRule = {};
        subBagRuleObject[value[1].concat(' ', value[2])] = Number(value[0]);
        return { ...acc, ...subBagRuleObject };
      }, {});

    const singleBagRule: [SingleRuleObj] | [] | any = [];
    singleBagRule[containerBag] = ruleRaw.includes('no other bags')
      ? {}
      : subBags;

    return singleBagRule;
  };

  const inputClean: SingleRuleObj = input.reduce(
    (acc, singleRule) => ({ ...acc, ...createRuleObject(singleRule) }),
    {}
  );

  const entriesContaining = (color: string): string[] => {
    const entryObject = Object.entries(inputClean).reduce(
      (acc: any, entry: any, index) => {
        if (entry[1][color]) {
          return [...acc, entry];
        }
        return acc;
      },
      []
    );

    return Object.keys(Object.fromEntries(entryObject));
  };

  const myBagColor = 'shiny gold';
  let alreadyCounted: string[] = [];

  let total = 0;
  const getBags = (colors: string[]) => {
    const newColors = colors.filter((color) => {
      return !alreadyCounted.includes(color);
    });

    total = total + newColors.length;
    const concatinatedColors = alreadyCounted.concat(newColors);
    alreadyCounted = Array.from(new Set(concatinatedColors));
    if (newColors.length === 0) {
      return;
    } else {
      colors.forEach((color) => {
        getBags(entriesContaining(color));
      });
    }
  };
  getBags(entriesContaining(myBagColor));

  const sumOfBagsForColor = (color: string): number => {
    return Object.values(inputClean[color]).reduce(
      (acc, numberOfBags) => acc + Number(numberOfBags),
      0
    );
  };

  let totalTwo = 0;

  const totalSum = (color: string, factor: number) => {
    totalTwo = totalTwo + sumOfBagsForColor(color) * factor;

    if (inputClean[color] !== {}) {
      Object.entries(inputClean[color]).forEach((entry) => {
        totalSum(entry[0], entry[1] * factor);
      });
    }
  };

  totalSum(myBagColor, 1);

  return <Outcome firstOutcome={total} secondOutcome={totalTwo} />;
};

export default Seven;
