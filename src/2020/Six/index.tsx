import React, { FunctionComponent } from 'react';
import { Outcome } from '../../components';
import input from './input';

interface Props {}

const Six: FunctionComponent<Props> = () => {
  const getUniqueLetters = (groupInput: string[]): string => {
    return Array.from(
      new Set(
        groupInput.reduce(
          (acc: string[], singleInput) => Array.from(singleInput).concat(acc),
          []
        )
      )
    ).join('');
  };

  const uniqueLettersInGroups = input.map((groupInput, index) => {
    return getUniqueLetters(groupInput);
  });

  const sumOfUniqueDeclaredItems = uniqueLettersInGroups
    .map((letters) => letters.length)
    .reduce((acc, length) => acc + length, 0);

  const getOverlappingUniqueLetters = (groupInput: string[]): string => {
    return groupInput
      .reduce((acc: string[], singleInput, index) => {
        if (index === 0) {
          return Array.from(singleInput);
        }
        const returnVal = Array.from(singleInput).filter((value) =>
          acc.includes(value)
        );
        return returnVal;
      }, [])
      .join('');
  };

  const overLappingLettersInGroup = input.map((groupInput, index) => {
    return getOverlappingUniqueLetters(groupInput);
  });

  const sumOfOverlappingItems = overLappingLettersInGroup
    .map((letters) => letters.length)
    .reduce((acc, length) => acc + length, 0);

  return (
    <Outcome
      firstOutcome={sumOfUniqueDeclaredItems}
      secondOutcome={sumOfOverlappingItems}
    />
  );
};

export default Six;
