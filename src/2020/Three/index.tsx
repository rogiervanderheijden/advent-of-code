import React, { FunctionComponent } from 'react';
import { Outcome } from '../../components';
import input from './input';

interface Props {}

const Three: FunctionComponent<Props> = () => {
  // const input = [
  //   '..##.......',
  //   '#...#...#..',
  //   '.#....#..#.',
  //   '..#.#...#.#',
  //   '.#...##..#.',
  //   '..#.##.....',
  //   '.#.#.#....#',
  //   '.#........#',
  //   '#.##...#...',
  //   '#...##....#',
  //   '.#..#...#.#',
  // ];
  const calcultateTrees = (
    horizontalStep: number,
    verticalStep: number
  ): number => {
    let pos = [0, 0];
    const fieldWidth = input[0].length;

    let treesEncountered = 0;

    input.forEach((line) => {
      const realLine = input[pos[1]];

      if (!realLine) return;
      const character = Array.from(realLine)[pos[0]];

      if (character === '#') {
        treesEncountered += 1;
      }

      const newHorizontalPos =
        pos[0] + horizontalStep > fieldWidth - 1
          ? pos[0] + horizontalStep - fieldWidth
          : pos[0] + horizontalStep;

      //update Position
      pos = [newHorizontalPos, pos[1] + verticalStep];
    });

    return treesEncountered;
  };
  const secondOutcome =
    calcultateTrees(1, 1) *
    calcultateTrees(3, 1) *
    calcultateTrees(5, 1) *
    calcultateTrees(7, 1) *
    calcultateTrees(1, 2);

  return (
    <Outcome
      firstOutcome={calcultateTrees(3, 1)}
      secondOutcome={secondOutcome}
    />
  );
};

export default Three;
