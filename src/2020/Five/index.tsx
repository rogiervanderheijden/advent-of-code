import React, { FunctionComponent } from 'react';
import { Outcome } from '../../components';
import input from './input';

interface Props {}

const Five: FunctionComponent<Props> = () => {
  const totalRowsLength = 128;
  const totalSeatsLength = 8;

  const rowNumber = (seatCode: string, rows: number): number => {
    const startRows = [0, rows - 1];
    const rowsLeft = Array.from(seatCode.substring(0, 7)).reduce(
      (acc, rowChar, index) => {
        if (rowChar === 'F') {
          return [acc[0], (acc[1] + 1 - acc[0]) / 2 - 1 + acc[0]];
        } else if (rowChar === 'B') {
          return [(acc[0] + acc[1] + 1) / 2, acc[1]];
        } else {
          throw new Error(`Row character code is ${rowChar}`);
        }
      },
      startRows
    );

    return rowsLeft[0];
  };

  const seatNumber = (seatCode: string, seats: number): number => {
    const startSeats = [0, seats - 1];
    let seatsLeft = startSeats;
    Array.from(seatCode.substring(7, 10)).forEach((seatChar, index) => {
      if (seatChar === 'L') {
        seatsLeft = [
          seatsLeft[0],
          (seatsLeft[1] + 1 - seatsLeft[0]) / 2 - 1 + seatsLeft[0],
        ];
      } else if (seatChar === 'R') {
        seatsLeft = [(seatsLeft[0] + seatsLeft[1] + 1) / 2, seatsLeft[1]];
      } else {
        throw new Error(`Seat character code is ${seatChar}`);
      }
    });

    return seatsLeft[0];
  };

  const seatId = (seatCode: string, rows: number, seats: number): number =>
    rowNumber(seatCode, rows) * 8 + seatNumber(seatCode, seats);

  const highestId = Math.max(
    ...input.map((seatCode) =>
      seatId(seatCode, totalRowsLength, totalSeatsLength)
    )
  );

  const seatsInRows: Array<number[]> = Array(totalRowsLength).fill([]);

  input.forEach((seatId) => {
    seatsInRows[rowNumber(seatId, totalRowsLength)] = [
      ...seatsInRows[rowNumber(seatId, totalRowsLength)],
      seatNumber(seatId, totalSeatsLength),
    ];
  });

  const yourSeat = seatsInRows
    .map((row, index) => {
      if (
        row &&
        row.length < 8 &&
        row.length > 0 &&
        seatsInRows[index - 1].length !== 0 &&
        seatsInRows[index + 1].length !== 0
      ) {
        const allSeats = [0, 1, 2, 3, 4, 5, 6, 7];
        return {
          rowNumber: index,
          seatNumber: allSeats.filter(
            (seatNumber) => row.indexOf(seatNumber) === -1
          )[0],
        };
      }
      return null;
    })
    .filter((row) => row)[0];

  const secondOutcome =
    yourSeat && yourSeat.rowNumber && yourSeat.seatNumber
      ? yourSeat.rowNumber * 8 + yourSeat.seatNumber
      : null;

  return <Outcome firstOutcome={highestId} secondOutcome={secondOutcome} />;
};

export default Five;
