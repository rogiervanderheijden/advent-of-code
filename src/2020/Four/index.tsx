import React, { FunctionComponent } from 'react';
import { Outcome } from '../../components';
import input from './input';

interface Props {}

const Three: FunctionComponent<Props> = () => {
  const isValid = (doc: any): boolean => {
    return (
      doc.byr && doc.iyr && doc.eyr && doc.hgt && doc.hcl && doc.ecl && doc.pid
    );
  };

  const validByr = (byr: number): boolean => {
    return byr >= 1920 && byr <= 2002;
  };
  const validIyr = (iyr: number): boolean => {
    return iyr >= 2010 && iyr <= 2020;
  };
  const validEyr = (eyr: number): boolean => {
    return eyr >= 2020 && eyr <= 2030;
  };
  const validHgt = (hgt: string): boolean => {
    const lastTwo = hgt.slice(-2);

    if (lastTwo !== 'cm' && lastTwo !== 'in') {
      return false;
    }

    const number = Number(hgt.replace(/\D/g, ''));

    if (lastTwo === 'cm') {
      return number >= 150 && number <= 193;
    }

    return number >= 59 && number <= 76;
  };

  const validHcl = (hcl: string): boolean => {
    return hcl.match(/^#[a-f0-9]{6}$/i) !== null;
  };

  const validEcl = (ecl: string): boolean => {
    return (
      ecl === 'amb' ||
      ecl === 'blu' ||
      ecl === 'brn' ||
      ecl === 'gry' ||
      ecl === 'grn' ||
      ecl === 'hzl' ||
      ecl === 'oth'
    );
  };

  const validPid = (pid: string): boolean => {
    return pid.length === 9 && /^\d+$/.test(pid);
  };

  const isValidTwo = (doc: any): boolean => {
    return (
      doc.byr &&
      validByr(Number(doc.byr)) &&
      doc.iyr &&
      validIyr(Number(doc.iyr)) &&
      doc.eyr &&
      validEyr(Number(doc.eyr)) &&
      doc.hgt &&
      validHgt(doc.hgt) &&
      doc.hcl &&
      validHcl(doc.hcl) &&
      doc.ecl &&
      validEcl(doc.ecl) &&
      doc.pid &&
      validPid(doc.pid)
    );
  };

  const countValidDocuments = input.reduce((acc, document) => {
    if (isValid(document)) {
      return (acc += 1);
    }
    return acc;
  }, 0);

  const countValidDocumentsTwo = input.reduce((acc, document) => {
    if (isValidTwo(document)) {
      return (acc += 1);
    }
    return acc;
  }, 0);

  return (
    <Outcome
      firstOutcome={countValidDocuments}
      secondOutcome={countValidDocumentsTwo}
    />
  );
};

export default Three;
