import React, { FunctionComponent } from 'react';
import { Outcome } from '../../components';
import input from './input';

interface Props {}

const Eight: FunctionComponent<Props> = () => {
  // const input = [
  //   ['nop', 0],
  //   ['acc', 1],
  //   ['jmp', 4],
  //   ['acc', 3],
  //   ['jmp', -3],
  //   ['acc', -99],
  //   ['acc', 1],
  //   ['jmp', -4],
  //   ['acc', 6],
  // ];
  let acc = 0;
  let pos = 0;
  let pastPositions: number[] = [];

  const lineActionOne = (
    line: [string, number],
    inputFromOutside: any[] = input
  ) => {
    if (pastPositions.find((pastPosition) => pastPosition === pos)) {
      return acc;
    }

    pastPositions = [...pastPositions, pos];

    switch (line[0]) {
      case 'acc':
        acc += line[1];
        pos += 1;
        break;
      case 'jmp':
        pos += line[1];
        break;
      case 'nop':
        pos += 1;
        break;
      default:
        throw new Error(`Code not recognized`);
    }

    lineActionOne(
      inputFromOutside[pos] as [string, number],
      inputFromOutside as [string, number][]
    );
  };

  lineActionOne(input[0] as [string, number], input as [string, number][]);

  // const swap = (swapPosition: number) => {
  //   const newInput = input.slice();

  //   if (newInput[swapPosition][0] === 'jmp') {
  //     newInput[swapPosition] = ['nop', newInput[swapPosition][1]];
  //   } else if (newInput[swapPosition][0] === 'nop') {
  //     newInput[swapPosition] = ['jmp', newInput[swapPosition][1]];
  //   }
  //   return newInput;
  // };

  // let accTwo = 0;
  // let posTwo = 0;
  // let pastPositionsTwo: number[] = [];

  // const lineActionTwo = (line: [string, number], inputFromOutside: any[]) => {
  //   console.log('line: ', line);
  //   if (pastPositionsTwo.find((pastPosition) => pastPosition === posTwo)) {
  //     // program is in a loop
  //     console.log('looped');
  //     return;
  //   } else if (inputFromOutside.length - 1 <= posTwo) {
  //     // position is higher that length of input
  //     console.log('finshed', inputFromOutside.length, posTwo, pastPositionsTwo);
  //     return;
  //   }

  //   pastPositionsTwo = [...pastPositionsTwo, posTwo];

  //   switch (line[0]) {
  //     case 'acc':
  //       accTwo += line[1];
  //       posTwo += 1;
  //       break;
  //     case 'jmp':
  //       posTwo += line[1];
  //       break;
  //     case 'nop':
  //       posTwo += 1;
  //       break;
  //     default:
  //       throw new Error(`Code not recognized`);
  //   }

  //   lineActionTwo(
  //     inputFromOutside[posTwo] as [string, number],
  //     inputFromOutside as [string, number][]
  //   );
  // };

  // input.forEach((_, index) => {
  //   accTwo = 0;
  //   posTwo = 0;
  //   pastPositionsTwo = [];
  //   if (input[index][0] !== 'acc') {
  //     const swappedInput = swap(index);
  //     lineActionTwo(swappedInput[0] as [string, number], swappedInput);
  //   }
  // });

  return <Outcome firstOutcome={acc} />;
};

export default Eight;
