import React, { FunctionComponent } from 'react';
import { Outcome } from '../../components';

import input from './input';

interface Props {}

const One: FunctionComponent<Props> = () => {
  const output: [number | undefined, number] = [0, 0];

  output[0] = input.find((inputValue: number) =>
    input.find((value: number) => {
      const testValue = value + inputValue === 2020;
      if (testValue) {
        output[1] = value;
      }
      return testValue;
    })
  );

  const firstOutcome = output[0] ? output[0] * output[1] : null;

  let secondOutcome;
  input.forEach((originalValue: number) => {
    input.forEach((firstVal: number) => {
      input.forEach((secondVal: number) => {
        if (originalValue + firstVal + secondVal === 2020) {
          secondOutcome = originalValue * firstVal * secondVal;
        }
      });
    });
  });

  return <Outcome firstOutcome={firstOutcome} secondOutcome={secondOutcome} />;
};

export default One;
