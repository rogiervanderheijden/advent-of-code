import React, { FunctionComponent } from 'react';

interface Props {}

const PageTitle: FunctionComponent<Props> = () => {
  return (
    <h1>
      <span className={'iconWrap'}>
        <span role="img" title="w00t" aria-label="star" className={'icon'}>
          👽
        </span>
        <span role="img" title="w00t" aria-label="star" className={'icon'}>
          🎄
        </span>
      </span>
      {'Advent of Code'}
      <span className={'iconWrap'}>
        <span role="img" title="w00t" aria-label="star" className={'icon'}>
          🎄
        </span>
        <span role="img" title="w00t" aria-label="star" className={'icon'}>
          👽
        </span>
      </span>
    </h1>
  );
};

export default PageTitle;
