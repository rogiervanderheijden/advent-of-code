import React, { FunctionComponent, ReactNode, useState } from 'react';

interface Props {
  headerCells: ReactNode;
  bodyCells: ReactNode;
  dayComponents: ReactNode;
  open: boolean;
  year: number;
}

const Year: FunctionComponent<Props> = ({
  headerCells,
  bodyCells,
  dayComponents,
  open,
  year,
}) => {
  const [isOpen, setIsOpen] = useState(open);
  return (
    <section className={'year'}>
      <div className={'yearTop'}>
        <h2>{year}</h2>
        <div className={'buttonWrap'}>
          <a href="https://bitbucket.org/rogiervanderheijden/advent-of-code/src">
            source code
          </a>
          <a href={`https://adventofcode.com/${year}/`}>challenges</a>
        </div>
      </div>
      {isOpen && (
        <div>
          <div className={'table-wrap'}>
            <table>
              <tbody>
                <tr>{headerCells}</tr>
                <tr>{bodyCells}</tr>
              </tbody>
            </table>
          </div>
          {dayComponents}
        </div>
      )}
      <button onClick={() => setIsOpen(!isOpen)}>
        {isOpen ? `hide ${year} ⇧` : `show ${year} ⇩`}
      </button>
    </section>
  );
};

export default Year;
