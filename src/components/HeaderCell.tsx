import React, { FunctionComponent } from 'react';

interface Props {
  number: number;
  stars?: number;
}

const HeaderCell: FunctionComponent<Props> = ({ number, stars }) => {
  return (
    <th>
      {number}
      {stars && stars > 0 && (
        <span role="img" title="w00t" aria-label="star">
          {stars === 1 ? '⭐' : '⭐⭐'}
        </span>
      )}
    </th>
  );
};

export default HeaderCell;
