import React, { FunctionComponent } from 'react';

interface Props {
  firstOutcome?: number | null;
  secondOutcome?: number | null;
}

const Outcome: FunctionComponent<Props> = ({ firstOutcome, secondOutcome }) => {
  return (
    <td>
      <div className={'outcome-wrapper'}>
        <span>1</span>
        <code>{firstOutcome ? firstOutcome : '...'}</code>
        {firstOutcome && (
          <span role="img" title="w00t" aria-label="star">
            ⭐
          </span>
        )}
      </div>
      <div className={'outcome-wrapper'}>
        <span>2</span>
        <code>{secondOutcome ? secondOutcome : '...'}</code>
        {secondOutcome && (
          <span role="img" title="w00t" aria-label="star">
            ⭐
          </span>
        )}
      </div>
    </td>
  );
};

export default Outcome;
