import React from 'react';
import './App.css';

import { PageTitle, HeaderCell, Year } from './components';

import TwentyNineteen from './2019';
import TwentyTwenty from './2020';

interface Props {}
interface State {
  open: {
    nineteen: boolean;
    twenty: boolean;
  };
}

class App extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      open: {
        nineteen: false,
        twenty: true,
      },
    };
  }
  render() {
    return (
      <>
        <PageTitle />
        <main>
          <Year
            open={true}
            year={2020}
            headerCells={
              <>
                <HeaderCell number={1} stars={2} />
                <HeaderCell number={2} stars={2} />
                <HeaderCell number={3} stars={2} />
                <HeaderCell number={4} stars={2} />
                <HeaderCell number={5} stars={2} />
                <HeaderCell number={6} stars={2} />
                <HeaderCell number={7} stars={2} />
                <HeaderCell number={8} stars={1} />
                <HeaderCell number={9} />
                <HeaderCell number={10} />
                <HeaderCell number={11} />
                <HeaderCell number={12} />
                <HeaderCell number={13} />
                <HeaderCell number={14} />
                <HeaderCell number={15} />
                <HeaderCell number={16} />
                <HeaderCell number={17} />
                <HeaderCell number={18} />
                <HeaderCell number={19} />
                <HeaderCell number={20} />
                <HeaderCell number={21} />
                <HeaderCell number={22} />
                <HeaderCell number={23} />
                <HeaderCell number={24} />
                <HeaderCell number={25} />
              </>
            }
            bodyCells={
              <>
                <TwentyTwenty.One />
                <TwentyTwenty.Two />
                <TwentyTwenty.Three />
                <TwentyTwenty.Four />
                <TwentyTwenty.Five />
                <TwentyTwenty.Six />
                <TwentyTwenty.Seven />
                <TwentyTwenty.Eight />
                <TwentyTwenty.Nine />
                <TwentyTwenty.TwentyFive />
                <TwentyTwenty.TwentyFive />
                <TwentyTwenty.TwentyFive />
                <TwentyTwenty.TwentyFive />
                <TwentyTwenty.TwentyFive />
                <TwentyTwenty.TwentyFive />
                <TwentyTwenty.TwentyFive />
                <TwentyTwenty.TwentyFive />
                <TwentyTwenty.TwentyFive />
                <TwentyTwenty.TwentyFive />
                <TwentyTwenty.TwentyFive />
                <TwentyTwenty.TwentyFive />
                <TwentyTwenty.TwentyFive />
                <TwentyTwenty.TwentyFive />
                <TwentyTwenty.TwentyFive />
                <TwentyTwenty.TwentyFive />
              </>
            }
            dayComponents={null}
          />
          <Year
            open={false}
            year={2019}
            headerCells={
              <>
                <HeaderCell number={1} stars={2} />
                <HeaderCell number={2} stars={1} />
                <HeaderCell number={3} stars={1} />
                <HeaderCell number={4} />
                <HeaderCell number={5} />
                <HeaderCell number={6} />
                <HeaderCell number={7} />
                <HeaderCell number={8} />
                <HeaderCell number={9} />
                <HeaderCell number={10} />
                <HeaderCell number={11} />
                <HeaderCell number={12} />
                <HeaderCell number={13} />
                <HeaderCell number={14} />
                <HeaderCell number={15} />
                <HeaderCell number={16} />
                <HeaderCell number={17} />
                <HeaderCell number={18} />
                <HeaderCell number={19} />
                <HeaderCell number={20} />
                <HeaderCell number={21} />
                <HeaderCell number={22} />
                <HeaderCell number={23} />
                <HeaderCell number={24} />
                <HeaderCell number={25} />
              </>
            }
            bodyCells={
              <>
                <TwentyNineteen.One />
                <TwentyNineteen.Two />
                <TwentyNineteen.Three />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
                <TwentyNineteen.TwentyFive />
              </>
            }
            dayComponents={<TwentyNineteen.ThreeCanvas />}
          />
        </main>
      </>
    );
  }
}

export default App;
