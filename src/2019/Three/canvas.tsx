import React from 'react';
import data from './data';

interface Props {}
interface State {
  wireOne: string[];
  wireTwo: string[];
  scale: number;
  scaleArray: number[];
}
interface DirectionInstruction {
  direction: string;
  steps: number;
}

class DayThreeDrawing extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      scaleArray: [0.05, 0.1, 1, 5, 10],
      scale: 0,
      wireOne: data[0],
      wireTwo: data[1],
    };
  }
  componentDidMount() {
    this.fillCanvas();
    this.centerCanvas();
  }
  private fillCanvas = (): void => {
    this.drawCanvas();
    this.drawLine(this.state.wireTwo, 'yellow');
    this.drawLine(this.state.wireOne, 'salmon');
  };
  private drawLine = (wireData: string[], color: string): void => {
    let currentPos = { x: 2000, y: 2000 };
    const canvas = this.refs.canvas as HTMLCanvasElement;
    const ctx = canvas.getContext('2d');
    if (!ctx) return;

    wireData.forEach((instruction) => {
      const { direction, steps } = this.directionInstruction(instruction);
      switch (direction) {
        case 'U':
          ctx.beginPath();
          ctx.strokeStyle = color;
          ctx.moveTo(currentPos.x, currentPos.y);
          ctx.lineTo(currentPos.x, currentPos.y - steps);
          ctx.stroke();
          currentPos.y -= steps;
          break;
        case 'D':
          ctx.beginPath();
          ctx.strokeStyle = color;
          ctx.moveTo(currentPos.x, currentPos.y);
          ctx.lineTo(currentPos.x, currentPos.y + steps);
          ctx.stroke();
          currentPos.y += steps;
          break;
        case 'L':
          ctx.beginPath();
          ctx.strokeStyle = color;
          ctx.moveTo(currentPos.x, currentPos.y);
          ctx.lineTo(currentPos.x - steps, currentPos.y);
          ctx.stroke();
          currentPos.x -= steps;
          break;
        case 'R':
          ctx.beginPath();
          ctx.strokeStyle = color;
          ctx.moveTo(currentPos.x, currentPos.y);
          ctx.lineTo(currentPos.x + steps, currentPos.y);
          currentPos.x += steps;
          ctx.stroke();
          break;
        default:
          throw new Error('No direction provided...');
      }
    });
  };

  private maxZoom = (): boolean => {
    return this.state.scaleArray.length - 1 === this.state.scale;
  };

  private zoom = (): void => {
    const canvas = this.refs.canvas as HTMLCanvasElement;
    const ctx = canvas.getContext('2d');
    const newScale = this.state.scale + 1;

    this.setState(
      {
        ...this.state,
        scale: this.maxZoom() ? 0 : newScale,
      },
      () => {
        if (!ctx) return;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        this.fillCanvas();
        if (this.state.scale === 0) {
          this.centerCanvas();
        }
      }
    );
  };

  private directionInstruction = (
    singleDirectionCode: string
  ): DirectionInstruction => ({
    direction: singleDirectionCode.slice(0, 1),
    steps:
      Number(singleDirectionCode.substr(1)) *
      this.state.scaleArray[this.state.scale],
  });

  render() {
    return (
      <>
        <h3>Day 3</h3>
        <div className={'canvas-wrap'} id="canvas-wrap">
          <canvas
            className={this.maxZoom() ? 'max-zoom' : ''}
            onClick={() => {
              this.zoom();
            }}
            id="canvas"
            ref="canvas"
            width="4000"
            height="4000"
          ></canvas>
        </div>
      </>
    );
  }
  private centerCanvas = () => {
    var wrap = document.getElementById('canvas-wrap');
    if (wrap) {
      wrap.scrollLeft = 2000 - wrap.offsetWidth / 2;
      wrap.scrollTop = 2000 - wrap.offsetHeight / 2;
    }
  };

  private drawCanvas = () => {
    const canvas = this.refs.canvas as HTMLCanvasElement;
    const ctx = canvas.getContext('2d');
    if (ctx) {
      for (let i = 1; i < 400; i++) {
        ctx.beginPath();
        ctx.strokeStyle = 'navy';
        ctx.moveTo(0, i * 10);
        ctx.lineTo(4000, i * 10);
        ctx.stroke();
      }
      for (let i = 1; i < 400; i++) {
        ctx.beginPath();
        ctx.strokeStyle = 'navy';
        ctx.moveTo(i * 10, 0);
        ctx.lineTo(i * 10, 4000);
        ctx.stroke();
      }
      for (let i = 1; i < 40; i++) {
        ctx.beginPath();
        ctx.strokeStyle = 'darkpurple';
        ctx.moveTo(i * 100, 0);
        ctx.lineTo(i * 100, 4000);
        ctx.stroke();
      }
      for (let i = 1; i < 40; i++) {
        ctx.beginPath();
        ctx.strokeStyle = 'darkpurple';
        ctx.moveTo(0, i * 100);
        ctx.lineTo(4000, i * 100);
        ctx.stroke();
      }
      ctx.beginPath();
      ctx.strokeStyle = 'silver';
      ctx.moveTo(0, 2000);
      ctx.lineTo(4000, 2000);
      ctx.stroke();
      ctx.beginPath();
      ctx.strokeStyle = 'silver';
      ctx.moveTo(2000, 0);
      ctx.lineTo(2000, 4000);
      ctx.stroke();

      ctx.fillStyle = 'lime';
      ctx.font = '18px Monospace';
      ctx.fillRect(1997, 1997, 6, 6);

      ctx.fillText('0,0', 2005, 1993);
      ctx.fillRect(2097, 2097, 6, 6);

      ctx.fillText(
        `${(1 / this.state.scaleArray[this.state.scale]) * 100},${
          (1 / this.state.scaleArray[this.state.scale]) * 100
        }`,
        2105,
        2093
      );
    }
  };
}

export default DayThreeDrawing;
