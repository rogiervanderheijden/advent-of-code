import React from 'react';
import data from './data';

import Outcome from '../../components/Outcome';
import ThreeCanvas from './canvas';

interface Props {}
interface State {
  firstWire: string[];
  secondWire: string[];
}

class Three extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = { firstWire: data[0], secondWire: data[1] };
  }
  render() {
    return <Outcome firstOutcome={209} />;
  }
}

export default Three;

export { ThreeCanvas };
