import One from './One';
import Two from './Two';
import Three, { ThreeCanvas } from './Three';
import TwentyFive from './TwentyFive';

export default { One, Two, Three, TwentyFive, ThreeCanvas };
