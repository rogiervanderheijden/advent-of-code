import React from 'react';
import Outcome from '../../components/Outcome';
import data from './data';

interface Props {}
interface State {
  firstOutcome: number | null;
  secondOutcome: number | null;
  data: number[];
}

class One extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      firstOutcome: null,
      secondOutcome: null,
      data: data,
    };
  }

  componentDidMount() {
    this.setState({
      firstOutcome: this.state.data.reduce(
        (acc, currentValue) => acc + this.calcSingleFuelModule(currentValue),
        0
      ),
      secondOutcome: this.state.data.reduce((acc, currentValue) => {
        let leftOverValue = this.calcSingleFuelModule(currentValue);

        const fuelModulesForCurrentValue: number[] = [leftOverValue];

        let recalculate = this.shouldRecalculate(leftOverValue);
        while (recalculate) {
          leftOverValue = this.calcSingleFuelModule(leftOverValue);
          fuelModulesForCurrentValue.push(leftOverValue);

          recalculate = this.shouldRecalculate(leftOverValue);
        }

        const total = fuelModulesForCurrentValue.reduce((acc, total) => {
          return acc + total;
        }, 0);
        return acc + total;
      }, 0),
    });
  }

  private calcSingleFuelModule = (input: number): number => {
    const outcome = Math.floor(input / 3) - 2;
    return outcome;
  };

  private shouldRecalculate = (valueLeft: number): boolean =>
    this.calcSingleFuelModule(valueLeft) > 0;

  render() {
    return (
      <Outcome
        firstOutcome={this.state.firstOutcome}
        secondOutcome={this.state.secondOutcome}
      />
    );
  }
}

export default One;
