import React from 'react';
import Outcome from '../../components/Outcome';
import data from './data';

interface Props {}

interface State {
  outcome: number | null;
  data: number[];
  target: number;
}

class Two extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = { outcome: null, data: data, target: 19690720 };
  }

  componentDidMount() {
    this.setState({
      data: this.dataReset(this.state.data),
    });
    this.run(0, this.state.data);
  }

  private dataReset(data: number[]): number[] {
    const updatedData = data;
    updatedData[1] = 12;
    updatedData[2] = 2;
    return updatedData;
  }

  private run = (pos: number, program: number[]): void => {
    const opCode = program[pos];

    if (opCode === 99) {
      this.setState({
        outcome: program[0],
      });
      return;
    } else if (opCode !== 1 && opCode !== 2) {
      //throw new Error('ERROR: Opcode not recognised, this shit is broken!!');
      console.log('ERROR: Opcode not recognised, this shit is broken!!');
    }

    const firstParameter = program[program[pos + 1]];
    const secondParameter = program[program[pos + 2]];

    program[program[pos + 3]] =
      opCode === 1
        ? firstParameter + secondParameter
        : firstParameter * secondParameter; // if opCode is not 1 it must be 2...

    pos = pos + 4;
    this.run(pos, program);
  };

  render() {
    return <Outcome firstOutcome={this.state.outcome} />;
  }
}

export default Two;
